const express = require('express');
const app = express();

app.use(express.static('./dist/edacy-angular'));
app.get('/*', (req, res) =>
  res.sendFile('index.html', {root: 'dist/edacy-angular'}),
);
app.listen(8080,'0.0.0.0',function () {
  console.log('START...')
});
