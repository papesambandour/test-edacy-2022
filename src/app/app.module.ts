import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BackofficeLayoutComponent } from './backoffice/components/layouts/backoffice-layout/backoffice-layout.component';
import { FormPrgramComponent } from './backoffice/components/pages/form-prgram/form-prgram.component';

@NgModule({
  declarations: [
    AppComponent,
    BackofficeLayoutComponent,
    FormPrgramComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
