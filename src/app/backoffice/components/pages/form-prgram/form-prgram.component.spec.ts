import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPrgramComponent } from './form-prgram.component';

describe('FormPrgramComponent', () => {
  let component: FormPrgramComponent;
  let fixture: ComponentFixture<FormPrgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormPrgramComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormPrgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
