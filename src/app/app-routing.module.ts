import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BackofficeLayoutComponent} from "./backoffice/components/layouts/backoffice-layout/backoffice-layout.component";
import {FormPrgramComponent} from "./backoffice/components/pages/form-prgram/form-prgram.component";

const routes: Routes = [
  {
    component:BackofficeLayoutComponent,
    path:'admin',
    children:[
      {
        component:FormPrgramComponent,
        path:''
      }
    ]
  },
  {
    path:"**",
    redirectTo:"admin",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
